// Operators
/*
	Add + = Sum
	Subtract - = Difference
	Multiply * = Product
	Divide / = Quotient
	Modulos % = Remainder
*/

function mod(){
	return 9 % 2;
}
console.log(mod());

// Assignment Operator (=)

/*
	+= (Addition)
	-= (Subtraction)
	*= (Multiplication)
	/= (Division)
	%= (Modulo)
*/

	let x = 1;

	let sum = 1;
	// sum = sum +1;
	sum += 1;

	console.log(sum);

// Increment(++) and Decrement(--)
// Operators that add or subtract values by 1 and reaasigns the value of the variable where the increment/decrement was applied to

let z = 1;
let increment = ++ z;
console.log("Result of pre-increment:" + increment);
console.log("Result of pre-increment:" + z);

// Post-increment
increment = z++;
console.log("Result of post-increment:" + increment);
console.log("Result of post-increment:" + z);

// Pre-derement
let decrement = --z;
console.log("Result of pre-decrement:" + decrement);
console.log("Result of pre-decrement:" + z);

//Post-decrement
decrement = z--;
console.log("Result of pre-decrement:" + decrement);
console.log("Result of pre-decrement:" + z);

// Comparison Operators

// Equality Operator (==)

let juan = 'juan';

console.log(1==1);
console.log(0 == false);
console.log('juan' == juan);
// Strict equality (===)
console.log(1 === true);

// Inequality Operator (!=)
console.log('Inequlity operator');
console.log(1 != 1);
console.log('Juan' != juan);

// Strict Inequality (!==)
console.log(0 !== false);

// Other Comparison Operators
/*
	> - Greater than
	< - Less that
	>= - Greater than or equal
	<= - Less than or equal  
*/

// Logical Operators 

let isLegalAge = true;
let isRegistered = false;
/*
	And Operator (&&) - return true if all operands are true
	true && true = true
	false && true = false
	true && false = false
	false && false = false

	Or Operator (||) - returns true if at least one operands is true
	true || true = true
	false || true = true
	true || false = true
	false || false = false

*/

// And operator

let allRequirementsMet = isLegalAge && isRegistered;
console.log("result of logical AND operator: " + allRequirementsMet);

// Or operator
let someRequirementsMet = isLegalAge || isRegistered
console.log("result of logical OR operator: " + someRequirementsMet);


// Selection Control Structures

/* IF Statement
	- execute a statement if a specified condition is true
	Syntax
	if(condition){
		statement/s;
	}
*/
let num = -1;

if(num < 0){
	console.log("Hello");
};

// create an if statement that will display the words "Welcome to Zuit" if a certain value is geater than or equal to 10.

let numA = 14
if(numA >= 10){
	console.log("Welcome to Zuit");
};


/* IF-ELSE statement
	- execute a statement if the previous condition returns false.
	Syntax;
	if(condition){
		statement/s
	}
	else{
		statement/s;
	}
	
*/


num = 5
if(num >= 10){
	console.log("Number is greater or equal to 10");
}
else{
	console.log("Number is not greater or equal to 10");
}

/*let age = parseInt(prompt("Please provide age: ")) ;

if(age > 59){
	alert("Senior age");
}
else{
	alert("Invalid age");
}*/

// IF-ELSEIF-ELSE statement

/*
	Syntax:
	if(condition){
		statement/s
	}
	else if(condition){
		statement/s
	}
	else if(condition){
		statement/s
	}
	.
	.
	.
	else{
		statement/s;
	}

	1 - Quezon City
	2 - Valenzuela City
	3 - Pasig City
	4 - Taguig
*/

/*
let city = parseInt(prompt("Enter a Number:"));

if (city === 1) {
	alert("Welcome to Quezon City")
}
else if (city === 2) {
	alert("Welcome to Valenzuela City")
}
else if (city === 3) {
	alert("Welcome to Pasig City")
}
else if (city === 4) {
	alert("Welcome to Taguig City")
}
else{
	alert("Invalid number")
}*/
let message = '';
function determineTyphonIntensity(windSpeed){
	if (windSpeed < 30) {
		return 'Not a typhon yet';
	}
	else if (windSpeed <= 61) {
		return 'Tropical depression detected';
	}
	else if (windSpeed >= 61 && windSpeed <= 88){
		return 'Tropical storm detected.';
	}
	else if (windSpeed >=89 && windSpeed <= 117){
		return 'Severe tropical storm detected';
	}
	else{
		return "Typhoon detected."
	}
}
message = determineTyphonIntensity(70);
console.log(message);

// Ternary operator
/* Syntax:
	(condition) ? ifTrue : ifFlase
*/
let ternaryResult = (1 < 18) ? true : false;
console.log("result of ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit '
}
function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit ';
}

let age = parseInt(prompt("What is your age?"));
let legalAge = (age >= 18 ? isOfLegalAge() : isUnderAge())
alert("Result of Ternary Operator in functions" + legalAge + ', ' + name);

// Switch statement
/* Syntax:
	switch (expression){
		case value1:
			statement/s;
			break;
		case value2:
			statement/s;
			break;
		case valueN:
			statement/s;
			break;
		default:
			statement/s;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();

switch (day){
	case 'sunday':
		alert("The color of the day is red");
		break;	
	case 'monday':
		alert("The color of the day is orange");
		break;
	case 'tuesday':
		alert("The color of the day is yellow");
		break;
	case 'wednesday':
		alert("The color of the day is green");
		break;
	case 'thursday':
		alert("The color of the day is blue");
		break;
	case 'friday':
		alert("The color of the day is indigo");
		break;
	case 'saturday':
		alert("The color of the day is violet");
		break;
	default:
		alert("Please input valid day");
}

// Try-Catch-Finally Statement - commonly used for error handling

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert('Intensity updates will show new alert. ')
	}
}

showIntensityAlert(56);